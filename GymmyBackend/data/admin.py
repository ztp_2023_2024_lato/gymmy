from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(UserData)
admin.site.register(ExerciseData)
admin.site.register(TrainingData)
admin.site.register(TrainingExerciseData)
admin.site.register(MuscleGroupData)
admin.site.register(PersonalAchievementsData)
admin.site.register(TrainingLogsData)