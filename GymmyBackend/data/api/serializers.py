from rest_framework import serializers
from django.contrib.auth import authenticate
from ..models import *

class UserDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserData
        fields = ('username', 'password', 'weight', 'height')

class MuscleGroupDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MuscleGroupData
        fields = ('name')

class ExerciseDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseData
        fields = ('name', 'description', 'muscleGroup')
    
class TrainingDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingData
        fields = ('name', 'description', 'exercise')
        
class TrainingExerciseDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingExerciseData
        fields = ('training', 'exercise', 'sets', 'repetitons', 'weight')

class PersonalAchievementsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalAchievementsData
        fields = ('user', 'exercise', 'maxWeight')

class TrainingLogsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingLogsData
        fields = ('user', 'exercise', 'date', 'sets', 'repetitons', 'weight')
        