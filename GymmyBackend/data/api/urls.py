from rest_framework import routers
from .views import *
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view


router = routers.DefaultRouter()
router.register(r'register', UserRegistrationViewSet, basename="register")

urlpatterns = [
    path('', include(router.urls)),
    path('login/', LoginView.as_view(), name='login'),
    path('user/update/<int:id>', UserUpdateAPIView.as_view(), name='user-update'),
    path('exercises/', ExerciseAPIView.as_view(), name='exercise-list'),
    path('exercises/create', ExerciseCreateAPIView.as_view(), name='exercise-create'),
    path('exercises/update/<int:id>', ExerciseUpdateAPIView.as_view(), name='exercise-update'),
    path('exercises/delete/<int:id>', ExerciseDeleteAPIView.as_view(), name='exercise-delete'),
]