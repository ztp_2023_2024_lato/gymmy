from rest_framework import viewsets, status, generics
from ..models import *
from .serializers import *
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from rest_framework.authtoken.models import Token
from .authentication import CustomModelBackend
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny


class UserRegistrationViewSet(viewsets.ModelViewSet):
    queryset = UserData.objects.all
    serializer_class = UserDataSerializer

    @csrf_exempt
    def list(self, request):
        if request.method == 'POST':
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                self.perform_create(serializer)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class LoginView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        user = get_object_or_404(UserData, username=request.data['username'])
        if not user.checkPassword(request.data['password']):
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)
        serializer = UserDataSerializer(user)
        print(user)
        return Response(serializer.data, status=status.HTTP_200_OK)

class UserUpdateAPIView(generics.UpdateAPIView):
    queryset=UserData.objects.all()
    serializer_class = UserDataSerializer
    lookup_field = 'id'
    
class ExerciseAPIView(generics.ListAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer

class ExerciseCreateAPIView(generics.CreateAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer

class ExerciseUpdateAPIView(generics.UpdateAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer
    lookup_field = 'id'

class ExerciseDeleteAPIView(generics.DestroyAPIView):
    queryset = ExerciseData.objects.all()
    lookup_field = 'id'