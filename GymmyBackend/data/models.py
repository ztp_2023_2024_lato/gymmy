from django.db import models

# Create your models here.
class UserData(models.Model):
    username = models.CharField(max_length = 20)
    password = models.CharField(max_length = 255)
    weight = models.FloatField()
    height = models.IntegerField()

    def checkPassword(self, password):
        if self.password == password:
            return True
        return False

class MuscleGroupData(models.Model):
    name = models.CharField(max_length=20)

class ExerciseData(models.Model):
    name = models.CharField(max_length = 20)
    description = models.CharField(max_length = 300)
    muscleGroup = models.ManyToManyField(MuscleGroupData)
    

class TrainingData(models.Model):
    name = models.CharField(max_length = 20)
    description = models.CharField(max_length = 300)
    exercise = models.ManyToManyField(ExerciseData, through='TrainingExerciseData')


class TrainingExerciseData(models.Model):
    training = models.ForeignKey(TrainingData, on_delete=models.DO_NOTHING)
    exercise = models.ForeignKey(ExerciseData, on_delete=models.DO_NOTHING)
    sets = models.IntegerField()
    repetitons = models.IntegerField()
    weight = models.IntegerField()

class PersonalAchievementsData(models.Model):
    user = models.ForeignKey(UserData, on_delete=models.DO_NOTHING)
    exercise = models.ForeignKey(ExerciseData, on_delete=models.DO_NOTHING)
    maxWeight = models.IntegerField()

class TrainingLogsData(models.Model):
    user = models.ForeignKey(UserData, on_delete=models.DO_NOTHING)
    exercise = models.ForeignKey(ExerciseData, on_delete=models.DO_NOTHING)
    date = models.DateField()
    sets = models.IntegerField()
    repetitons = models.IntegerField()
    weight = models.IntegerField()